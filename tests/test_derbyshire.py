import os
import unittest
from unittest import mock

import testscenarios
from errbot.backends import test


import derbyshire


class DerbyshireBotCommandsTestCase(unittest.TestCase):


    def setUp(self):
        super().setUp()
        self.testbot = test.TestBot(
            extra_plugin_dir=os.path.join(os.path.dirname(__file__), '..'),
            extra_config={'CORE_PLUGINS': None})
        self.testbot.start()
        self.addCleanup(self.testbot.stop)


    def test_reply(self):
        self.testbot.push_message(':(')
        self.assertEqual(
            'Te mando un abrazo :(',
            self.testbot.pop_message())


# Fake randomness
# XXX: patching this way globally is ugly, breaks the execution in parallel.
# --elopio - 20201005
current_index = 0
mock_random_index = 0


def mock_choice(sequence):
    """Return the items in order, instead of randomly."""
    global current_index, mock_random_index
    current_index = mock_random_index
    mock_random_index = mock_random_index + 1
    return sequence[current_index]


def mock_randrange(*_):
    """Always return 0, which ends up meaning to always reply."""
    return 0


class DerbyshireRegexTestCase(testscenarios.TestWithScenarios):

    scenarios = [
        ('hola', {
            'message': 'dummy hola dummy',
            'expected_replies': ['o/', 'holi']}),
        ('Hola', {
            'message': 'dummy Hola dummy',
            'expected_replies': ['o/']}),

        ('el jaqueado', {
            'message': 'dummy el jaqueado dummy',
            'expected_replies': ['aww <3']}),
        ('eljaqueado', {
            'message': 'dummy eljaqueado dummy',
            'expected_replies': ['aww <3']}),

        ('bot', {
            'message': 'dummy bot dummy',
            'expected_replies': [
                '¿Qué ondas? ¿Hay un bot por acá?',
                '¿No querrás decir cyborg?',
                '¿a quién le hablás?'
            ]}),


        (':(', {
            'message': 'dummy :( dummy',
            'expected_replies': ['Te mando un abrazo :(']}),

        ('bunqueer', {
            'message': 'dummy bunqueer dummy',
            'expected_replies': [(
                'ey, que chiva el bunqueer comp+s, '
                '¿nos juntamos por allá en un rato?')]}),

        ('http', {
            'message': 'dummy http://dummy dummy',
            'expected_replies': [(
                '¡Qué interesante! Mandalo también a '
                'https://bunqueer.jaquerespeis.org')]}),
        ('https', {
            'message': 'dummy https://dummy dummy',
            'expected_replies': [(
                '¡Qué interesante! Mandalo también a '
                'https://bunqueer.jaquerespeis.org')]}),
        ('https', {
            'message': 'dummy https://bunqueer.jaquerespeis.org dummy',
            'expected_replies': [None]}),

        ('open source', {
            'message': 'dummy open source dummy',
            'expected_replies': ['¿no querrás decir software libre?']}),
        ('opensource', {
            'message': 'dummy opensource dummy',
            'expected_replies': ['¿no querrás decir software libre?']}),
        ('código abierto', {
            'message': 'dummy código abierto dummy',
            'expected_replies': ['¿no querrás decir software libre?']}),
        ('codigo abierto', {
            'message': 'dummy codigo abierto dummy',
            'expected_replies': ['¿no querrás decir software libre?']}),
        ('códigoabierto', {
            'message': 'dummy códigoabierto dummy',
            'expected_replies': ['¿no querrás decir software libre?']}),
        ('codigoabierto', {
            'message': 'dummy codigoabierto dummy',
            'expected_replies': ['¿no querrás decir software libre?']}),

        ('hay que', {
            'message': 'dummy hay que dummy',
            'expected_replies': ['che que buena idea, por que no la haces?']}),
        ('habría que', {
            'message': 'dummy habría que dummy',
            'expected_replies': ['che que buena idea, por que no la haces?']}),
        ('habria que', {
            'message': 'dummy habria que dummy',
            'expected_replies': ['che que buena idea, por que no la haces?']}),

        ('windows', {
            'message': 'dummy windows dummy',
            'expected_replies': ['eso en GNU/Linux no pasa xD']}),

        ('pyhon', {
            'message': 'dummy python dummy',
            'expected_replies': ['python love <3']}),

        ('torrent', {
            'message': 'dummy torrent dummy',
            'expected_replies': [
                'si no torrenteamos, la cultura se netflixea']})
    ]


    def setUp(self):
        super().setUp()
        self.derbyshire = derbyshire.Derbyshire(mock.MagicMock())
        global mock_random_index
        mock_random_index = 0


    @mock.patch('random.choice', side_effect=mock_choice)
    @mock.patch('random.randrange', side_effect=mock_randrange)
    def test_get_message_reply(self, *_):
        for expected_reply in self.expected_replies:
            reply = self.derbyshire._get_message_reply(self.message)
            self.assertEqual(reply, expected_reply)

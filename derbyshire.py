import collections
import enum
import random
import re

import errbot


class DerbyshireError(Exception):
    """Base exception class for derbyshire errors."""


class DerbyshireUnsupportedActionFrequencyError(DerbyshireError):
    """Exception raised when the action frequency is not supported."""


class ActionFrequency(enum.Enum):
    NEVER = 0  # Disabled.
    ALWAYS = 1
    SOMETIMES = 2  # Execute the action with a probability of 1/5.


class Derbyshire(errbot.BotPlugin):

    # Actions dictionary.
    # The key is the regular expression that will trigger the action.
    # The item is a tupple of (Action Frequency, List of replies).
    # The reply will be taken randomly from the list of replies.
    ACTIONS = collections.OrderedDict([
        ('[Hh]ola', (
            ActionFrequency.ALWAYS,
            ['o/', 'holi'])),

        ('el ?jaqueado', (
            ActionFrequency.ALWAYS,
            ['aww <3'])),

        ('bot', (
            ActionFrequency.ALWAYS,
            ['¿Qué ondas? ¿Hay un bot por acá?',
             '¿No querrás decir cyborg?',
             '¿a quién le hablás?',
            ])),

        (':\(', (
            ActionFrequency.ALWAYS,
            ['Te mando un abrazo :('])),

        (' bunqueer', (
            ActionFrequency.ALWAYS,
            ['ey, que chiva el bunqueer comp+s, '
             '¿nos juntamos por allá en un rato?'
            ])),

        ('https?:\/\/(?!bunqueer.jaquerespeis.org)', (
            ActionFrequency.SOMETIMES,
            ['¡Qué interesante! Mandalo también a '
             'https://bunqueer.jaquerespeis.org'])),

        ('(open ?source)|(c[óo]digo ?abierto)', (
            ActionFrequency.ALWAYS,
            ['¿no querrás decir software libre?'])),

        ('ha(y|br[íi]a) que', (
            ActionFrequency.ALWAYS,
            ['che que buena idea, por que no la haces?'])),

        ('windows', (
            ActionFrequency.ALWAYS,
            ['eso en GNU/Linux no pasa xD'])),

        ('python', (
            ActionFrequency.ALWAYS,
            ['python love <3'])),

        ('torrent', (
            ActionFrequency.ALWAYS,
            ['si no torrenteamos, la cultura se netflixea']))
    ])


    def callback_message(self, message):
        reply = self._get_message_reply(message.body)
        if reply:
            recipient = message.frm if message.is_direct else message.to
            self.send(recipient, reply)


    def _get_message_reply(self, message):
        for regular_expression, (action_frequency, replies) in \
            self.ACTIONS.items():
                if re.search(regular_expression, message):
                    if self._should_act(action_frequency):
                        return random.choice(replies)


    def _should_act(self, action_frequency):
        if action_frequency == ActionFrequency.ALWAYS:
            return True
        elif action_frequency == ActionFrequency.SOMETIMES:
            # 1 in 5 chance of returning true.
            # Randrange returns a random value between 0 and 5.
            # When it is 0, casting to bool returns false.
            # By negating it, it will return true with a
            # probability of 1/5.
            return not bool(random.randrange(5))
        elif action_frequency == ActionFrequency.NEVER:
            return False
        else:
            raise DerbyshireUnsupportedActionFrequencyError(action_frequency)
